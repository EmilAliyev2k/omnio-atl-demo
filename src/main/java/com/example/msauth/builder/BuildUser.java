package com.example.msauth.builder;

import com.example.msauth.dao.entity.UserEntity;
import com.example.msauth.model.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

public enum BuildUser {
    BUILD_USER;

    public final UserDto buildDto(UserEntity user) {
        return UserDto.builder()
                .name(user.getFirstName())
                .surname(user.getLastName())
                .username(user.getUsername())
                .email(user.getEmail())
                .jobTitle(user.getJobTitle())
                .role(user.getRole())
                .build();
    }
    public final List<UserDto> buildDtoList(List<UserEntity> userList) {
        List<UserDto> userDtoList = new ArrayList<>();
        for (int i = 0; i < userList.size(); i++) {
            userDtoList.add(buildDto(userList.get(i)));
        }
        return userDtoList;

    }
}
