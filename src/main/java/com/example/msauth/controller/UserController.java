package com.example.msauth.controller;

import com.example.msauth.dao.entity.UserEntity;
import com.example.msauth.model.dto.UpdatePassRequest;
import com.example.msauth.model.dto.UserDto;
import com.example.msauth.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService profileService;

    @GetMapping("/get-user-by-id/{id}")
    public UserDto getUserProfile(@PathVariable Long id) {
        return profileService.getUserById(id);

    }
    @GetMapping("/get-all-users")
    public List<UserDto> getAllUsers() {
        return profileService.getAllUsers();

    }
    @PutMapping("/update-user/{id}")
    public UserDto updateUser(@PathVariable Long id, @RequestBody UserEntity user) {
        return profileService.updateUser(id, user);

    }
    @PutMapping("/update-pass/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updatePassword(@PathVariable Long id, @RequestBody UpdatePassRequest updatePassRequest) {
        profileService.updatePassword(id, updatePassRequest);
    }
    @DeleteMapping("/delete-user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable Long id) {
        profileService.deleteUserById(id);
    }
    @DeleteMapping("/delete-user/{nickname}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable String nickname) {
        profileService.deleteUser(nickname);
    }
    @GetMapping("/get-users-with-pagination/{offset}/{pageSize}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<UserDto> getUsersWithPagination(@PathVariable int offset, @PathVariable int pageSize) {
        return profileService.getWithPagination(offset, pageSize);


    }
}
