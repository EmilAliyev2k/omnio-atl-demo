package com.example.msauth.exception.handler;
import com.example.msauth.exception.InvalidPasswordException;
import com.example.msauth.exception.NotFoundUserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
;

@RestController
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(NotFoundUserException.class)
    public ErrorResponse handle(NotFoundUserException exception) {
        log.error("ClientException: ", exception);
        return ErrorResponse.of(exception);
    }
    @ExceptionHandler(InvalidPasswordException.class)
    public ErrorResponse handle(InvalidPasswordException exception) {
        log.error("ClientException: ", exception);
        return ErrorResponse.of(exception);
    }
}
