package com.example.msauth.model;

public enum Role {
    ADMIN,
    USER
}
