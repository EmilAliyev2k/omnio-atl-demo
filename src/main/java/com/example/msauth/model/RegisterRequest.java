package com.example.msauth.model;

import com.example.msauth.annotation.ValidEmail;
import com.example.msauth.annotation.ValidPassword;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class RegisterRequest {
    @NotBlank
    String firstName;
    @NotBlank
    String lastName;
    @NotBlank
    String userName;
    @ValidEmail
    String email;
    String jobTitle;
    @ValidPassword
    String password;
    Role role;
}