package com.example.msauth.model.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OperationMessage {
    USER_NOT_FOUND("user.NotFound"),
    INVALID_PASSWORD("invalid.Password");
    private final String message;
}
