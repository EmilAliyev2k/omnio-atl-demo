package com.example.msauth.service;

import com.example.msauth.dao.entity.UserEntity;
import com.example.msauth.exception.NotFoundUserException;
import com.example.msauth.model.dto.UpdatePassRequest;
import com.example.msauth.model.dto.UserDto;
import java.util.List;

public interface UserService {
    UserDto getUserById(Long id) throws NotFoundUserException;
    UserDto updateUser(Long id, UserEntity user) throws NotFoundUserException;
    void updatePassword(Long id, UpdatePassRequest updatePassRequest) throws NotFoundUserException;
    void deleteUserById(Long id) ;
    void deleteUser(String username) ;
    List<UserDto> getWithPagination(int page, int size);

    List<UserDto> getAllUsers();
}
