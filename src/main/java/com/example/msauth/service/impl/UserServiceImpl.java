package com.example.msauth.service.impl;

import com.example.msauth.exception.NotFoundUserException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.msauth.dao.entity.UserEntity;
import com.example.msauth.dao.repository.UserRepository;
import com.example.msauth.exception.InvalidPasswordException;
import static com.example.msauth.builder.BuildUser.BUILD_USER;
import static com.example.msauth.model.consts.OperationMessage.USER_NOT_FOUND;
import static com.example.msauth.model.consts.OperationMessage.INVALID_PASSWORD;
import com.example.msauth.model.dto.UpdatePassRequest;
import com.example.msauth.model.dto.UserDto;
import com.example.msauth.service.UserService;
import java.util.List;
import java.util.Locale;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final MessageSource messageSource;

    @Override
    public UserDto getUserById(Long id) {
        Locale locale = LocaleContextHolder.getLocale();
        Object[] objs1 = new Object[1];
        objs1[0] = id;
        return BUILD_USER.buildDto(userRepository.findById(id)
                .orElseThrow(() ->
                        new NotFoundUserException(messageSource.getMessage(USER_NOT_FOUND.getMessage(), objs1, locale))));


    }

    @Override
    public UserDto updateUser(Long id, UserEntity user) {
        UserEntity userFromDb = userRepository.findById(id).orElseThrow(
                ()->new NotFoundUserException("User with this " + id + "not found"));
        userFromDb.setFirstName(user.getFirstName());
        userFromDb.setLastName(user.getLastName());
        userFromDb.setUserName(user.getUsername());
        userFromDb.setEmail(user.getEmail());
        userFromDb.setJobTitle(user.getJobTitle());
        userFromDb.setRole(user.getRole());
        userRepository.save(userFromDb);
        return BUILD_USER.buildDto(userFromDb);

    }

    @Override
    public void updatePassword(Long id, UpdatePassRequest updatePassRequest) {
        Locale locale = LocaleContextHolder.getLocale();
        UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundUserException("User with this " + id + " is not found"));
        if (passwordEncoder.matches(updatePassRequest.getCurrentPassword(), user.getPassword())) {
            user.setPassword(passwordEncoder.encode(updatePassRequest.getNewPassword()));
            userRepository.save(user);
        } else {
            throw new InvalidPasswordException(messageSource.getMessage(INVALID_PASSWORD.getMessage(), null, locale));

        }


    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void deleteUser(String username) {
        userRepository.deleteByUserName(username);
    }


    @Override
    public List<UserDto> getWithPagination(int offset, int pageSize) {
        return BUILD_USER.buildDtoList(userRepository.findAll(PageRequest.of(offset, pageSize)).toList());
    }
    @Override
    public UserDto getUserByUserName(String userName) {
        UserEntity userEntity = userRepository.findByUserName(userName)
                .orElseThrow(() -> new NotFoundUserException(userName));
        return BUILD_USER.buildDto(userEntity);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return BUILD_USER.buildDtoList(userRepository.findAll());
    }
}
