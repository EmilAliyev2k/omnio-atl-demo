package com.example.msauth.service;
import com.example.msauth.dao.entity.UserEntity;
import com.example.msauth.dao.repository.UserRepository;
import com.example.msauth.exception.NotFoundUserException;
import com.example.msauth.model.AuthenticationRequest;
import com.example.msauth.model.AuthenticationResponse;
import com.example.msauth.model.RegisterRequest;
import com.example.msauth.model.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.example.msauth.builder.BuildUser.BUILD_USER;


@Service
@RequiredArgsConstructor

public class AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public UserDto register(RegisterRequest request) {
        var user = UserEntity.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .userName(request.getUserName())
                .email(request.getEmail())
                .jobTitle(request.getJobTitle())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .build();
        userRepository.save(user);
        return BUILD_USER.buildDto(user);


    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
                (request.getUsername(), request.getPassword()));
        var user = userRepository.findByUserName(request.getUsername())
                .orElseThrow(() -> new NotFoundUserException("Username is invalid"));
        var jwtToken = jwtService.generateToken(String.valueOf(user));
        return AuthenticationResponse.builder().token(jwtToken).build();
    }

}

